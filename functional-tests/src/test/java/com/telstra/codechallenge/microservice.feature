# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to know if my spring boot application is running


    Scenario: Fetch an oldest zero follower user account
    Given url microserviceUrl
    And path '/fetch/zero/follower/account'
	And params {'count':'1'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    And match response ==  [{  login : '#string', id : '#number', html_url : '#string'  }]
   
    Scenario: Fetch 5 oldest zero follower user account
    Given url microserviceUrl
    And path '/fetch/zero/follower/account'
    And params {'count':'5'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { login : '#string', id : '#number', html_url : '#string' }
    And match response ==  '#[5] quoteSchema'
   
    Scenario: Fetch zero oldest zero follower user account
    Given url microserviceUrl
    And path '/fetch/zero/follower/account'
    And params {'count':'0'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    And match response ==
    """
    []
    """
   
    Scenario: Fetch all oldest zero follower user account
    Given url microserviceUrl
    And path '/fetch/zero/follower/account'
    And params {'count':'1000'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { login : '#string', id : '#number', html_url : '#string' }
    And match response ==  '#[30] quoteSchema'
   
    Scenario: Fetch oldest zero follower user account with bad input
    Given url microserviceUrl
    And path '/fetch/zero/follower/account'
    And params {'count':'abc'}
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { login : '#string', id : '#number', html_url : '#string' }
    And match response !=  'quoteSchema'
   
    Scenario: Fetch oldest zero follower user account with invalid url
    Given url microserviceUrl
    And path '/fetch/zero/follower/account/count'
    When method GET
    Then status 404