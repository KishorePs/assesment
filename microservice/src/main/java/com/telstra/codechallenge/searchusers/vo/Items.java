package com.telstra.codechallenge.searchusers.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Items {

  private int id;
  private String login;
  @JsonProperty("html_url")
  private String htmlurl;

}
