package com.telstra.codechallenge.searchusers.controller;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.JsonNode;
import com.telstra.codechallenge.searchusers.service.SearchUserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class SearchUserController {

  @Autowired
  SearchUserService searchUserService;
  
  @GetMapping(value = "${fetch-oldest-users}",
	      produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<JsonNode> getOldestUsers(@RequestParam(value = "count", defaultValue = "1")int count) {

	  log.debug("Request received in getoldestUsers()..");
	  return searchUserService.fetchOldestUsers(count);

  }
}
