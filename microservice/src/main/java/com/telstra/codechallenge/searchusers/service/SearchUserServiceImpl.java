package com.telstra.codechallenge.searchusers.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telstra.codechallenge.searchusers.util.SearchUsersConstants;
import com.telstra.codechallenge.searchusers.vo.Items;
import com.telstra.codechallenge.searchusers.vo.SearchUserResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SearchUserServiceImpl implements SearchUserService{
  
  @Autowired
  private RestTemplate restTemplate;
  
  @Value("${github.api.prefix}")
  private String gitHubApiPrefix;
  
  @Value("${github.api.search.user.endpoint}")
  private String searchUserEndpoint;
  
  @Value("${github.api.param.qualifier.value}")
  private String qualifierValue;
  
  @Value("${github.api.param.sort.value}")
  private String sortValue;
  
  @Value("${github.api.param.order.value}")
  private String ordervalue;
  
  @Value("${github.api.param.perpage.value}")
  private String perPageValue;
  
  @Override
  public ResponseEntity fetchOldestUsers(int count) {

    log.debug("Inside fetchOldestUsers()..");
    
    ResponseEntity<JsonNode> searchUserResponse = null;
    List<Items> listOfItems = new ArrayList<Items>();
    ObjectMapper mapper = new ObjectMapper();
	String countPerPage = null;
	JsonNode node = null;
	
    Integer pageNo = count/100;
    Integer perPage = count%100;
    
    try {

      HttpHeaders header = new HttpHeaders();
      header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
      header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());

      HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);

      for(int i = 0; i <= pageNo; i++) {

        if(i==pageNo) {
          countPerPage = perPage.toString();
          if(perPage == 0) {
        	  break;
          }
        } else {
          countPerPage = perPageValue;
    	}

        URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix).path(searchUserEndpoint)
            .queryParam(SearchUsersConstants.QUALIFIER, qualifierValue)
            .queryParam(SearchUsersConstants.SORT, sortValue)
            .queryParam(SearchUsersConstants.ORDER, ordervalue)
            .queryParam(SearchUsersConstants.PERPAGE, countPerPage)
            .queryParam(SearchUsersConstants.PAGE, 1 + i).build().encode().toUri();

        log.info("GitHub API call to fetch Oldest Users..");
        searchUserResponse = restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class);

        if(searchUserResponse.getStatusCode() == HttpStatus.OK) {

          log.info("Received successful response from GitHub API.");
          JsonNode response = searchUserResponse.getBody();
          SearchUserResponse<?> accountsList = mapper.treeToValue(response, SearchUserResponse.class);
          listOfItems.addAll(accountsList.getItems());
        }

      }

      log.info("Retrieved number of users account: {}",listOfItems.size());

      String jsonString = mapper.writeValueAsString(listOfItems);
      node = mapper.readTree(jsonString);

    } catch (HttpClientErrorException e) {
      log.error(e.getStatusText(),e);
      return ResponseEntity.status(e.getStatusCode()).body(e.getStatusText());
      
    } catch (JsonProcessingException e) {
      log.error("Error occured when filtering JSONResponse.{}",e);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Error occured when filtering JSONResponse.");
      
    } catch (Exception ex) {
      log.error("GitHub API call failed for getting Details.{}",ex);
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("GitHub API call failed for getting Details.");
    }
    
	return ResponseEntity.ok(node);
  }
}