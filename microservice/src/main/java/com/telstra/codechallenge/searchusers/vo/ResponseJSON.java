package com.telstra.codechallenge.searchusers.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ResponseJSON {

  private int id;
  private String login;
  @JsonProperty("html_url")
  private String htmlUrl;

}
