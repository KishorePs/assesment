package com.telstra.codechallenge.searchusers.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;

@Service
public interface SearchUserService {

	public ResponseEntity<JsonNode> fetchOldestUsers(int count);

}
