package com.telstra.codechallenge.searchusers.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchUserResponse<JsonArray> {

  private Long total_count;
  private Boolean incomplete_results;
  private List<Items> items;

}
