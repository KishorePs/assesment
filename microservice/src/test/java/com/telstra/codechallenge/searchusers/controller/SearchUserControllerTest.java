package com.telstra.codechallenge.searchusers.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import com.fasterxml.jackson.databind.JsonNode;
import com.telstra.codechallenge.searchusers.controller.SearchUserController;
import com.telstra.codechallenge.searchusers.util.SearchUserUtil;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SearchUserControllerTest {

  @LocalServerPort
  private int port;

  @Mock
  private SearchUserController searchUserController;
  
  @Mock
  private SearchUserUtil searchUserUtil;
  
  @Test
  public void getOldestUsersTest() throws Exception {
	  
    ResponseEntity<JsonNode> mockedResponse = SearchUserUtil.getUserDetails();
    when(searchUserController.getOldestUsers(1)).thenReturn(mockedResponse);

    ResponseEntity<JsonNode> response = searchUserController.getOldestUsers(1);
    assertEquals(mockedResponse, response);
    
  }

}
