package com.telstra.codechallenge.searchusers.util;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SearchUserUtil {

  public static ResponseEntity<JsonNode> getSearchUsersJSON() throws Exception {
		
    ObjectMapper objectMapper = new ObjectMapper();
    String jsonResponse = "{ \"items\": [ { \"login\": \"errfree\",\"id\": 44,\"html_url\": \"https://github.com/errfree\"}]}";
    JsonNode jsonNode = objectMapper.readTree(jsonResponse);
    return ResponseEntity.ok(jsonNode);
    
  }

  public static ResponseEntity<JsonNode> getUserDetails() throws Exception {
		
	    ObjectMapper objectMapper = new ObjectMapper();
	    String jsonResponse = "[ { \"login\": \"errfree\",\"id\": 44,\"html_url\": \"https://github.com/errfree\"}]";
	    JsonNode jsonNode = objectMapper.readTree(jsonResponse);
	    return ResponseEntity.ok(jsonNode);
	    
	  }
  
  public static ResponseEntity<String> getErrorResponse() {
	  return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("GitHub API call failed for getting Details.");
  }
  
  public static ResponseEntity<String> getHttpErrorResponse() {
	  return ResponseEntity.status(HttpStatus.FORBIDDEN).body("rate limit exceeded");
  }
  
}
