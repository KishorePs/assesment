package com.telstra.codechallenge.searchusers.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.net.URI;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.telstra.codechallenge.searchusers.service.SearchUserServiceImpl;
import com.telstra.codechallenge.searchusers.util.SearchUserUtil;
import com.telstra.codechallenge.searchusers.util.SearchUsersConstants;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SearchUserServiceTest {

  @LocalServerPort
  private int port;
  
  @Mock
  private SearchUserUtil searchUserUtil;

  @Mock
  private RestTemplate restTemplate;
  
  @Value("${github.api.prefix}")
  private String gitHubApiPrefix;
  
  @Value("${github.api.search.user.endpoint}")
  private String searchUserEndpoint;
  
  @Value("${github.api.param.qualifier.value}")
  private String qualifierValue;
  
  @Value("${github.api.param.sort.value}")
  private String sortValue;
  
  @Value("${github.api.param.order.value}")
  private String ordervalue;
  
  @Value("${github.api.param.perpage.value}")
  private String perPageValue;
  
  @InjectMocks
  private SearchUserServiceImpl searchUserServiceImpl = new SearchUserServiceImpl();
  
  @BeforeEach
  public void before() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(searchUserServiceImpl, "gitHubApiPrefix", "https://api.github.com");
		ReflectionTestUtils.setField(searchUserServiceImpl, "searchUserEndpoint", "/search/users");
		ReflectionTestUtils.setField(searchUserServiceImpl, "qualifierValue", "followers:0");
		ReflectionTestUtils.setField(searchUserServiceImpl, "sortValue", "joined");
		ReflectionTestUtils.setField(searchUserServiceImpl, "ordervalue", "asc");
		ReflectionTestUtils.setField(searchUserServiceImpl, "perPageValue", "100");
  }
  
  @Test
  public void getOldestUsersTest() throws Exception {

    ResponseEntity mockedJSON = SearchUserUtil.getSearchUsersJSON();
    ResponseEntity expectedResponse = SearchUserUtil.getUserDetails();
    
    HttpHeaders header = new HttpHeaders();
    header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
    header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());
    
    HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);
    
    URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix)
      .path(searchUserEndpoint)
      .queryParam(SearchUsersConstants.QUALIFIER, qualifierValue)
      .queryParam(SearchUsersConstants.SORT, sortValue)
      .queryParam(SearchUsersConstants.ORDER, ordervalue)
      .queryParam(SearchUsersConstants.PERPAGE, 1)
      .queryParam(SearchUsersConstants.PAGE, 1)
      .build().encode().toUri();  

    when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
      .thenReturn(mockedJSON);
    
    ResponseEntity response = searchUserServiceImpl.fetchOldestUsers(1);
    assertEquals(expectedResponse, response);
  }
  
  @Test
  public void getErrorResponse() throws Exception {

    ResponseEntity mockedJSON = SearchUserUtil.getUserDetails();
    ResponseEntity expectedResponse = SearchUserUtil.getErrorResponse();

    HttpHeaders header = new HttpHeaders();
    header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
    header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());

    HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);

    URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix)
    		.path(searchUserEndpoint)
    		.queryParam(SearchUsersConstants.QUALIFIER, qualifierValue)
    		.queryParam(SearchUsersConstants.SORT, sortValue)
    		.queryParam(SearchUsersConstants.ORDER, ordervalue)
    		.queryParam(SearchUsersConstants.PERPAGE, perPageValue)
    		.build().encode().toUri();  

    when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
      .thenReturn(expectedResponse);

    ResponseEntity response = searchUserServiceImpl.fetchOldestUsers(1);
    assertEquals(expectedResponse, response);
  }
  
  @Test
  public void getHttpErrorResponse() throws Exception {
	  
	    ResponseEntity expectedResponse = SearchUserUtil.getHttpErrorResponse();

	    HttpHeaders header = new HttpHeaders();
	    header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
	    header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());

	    HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);

	    URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix)
	    		.path(searchUserEndpoint)
	    		.queryParam(SearchUsersConstants.QUALIFIER, qualifierValue)
	    		.queryParam(SearchUsersConstants.SORT, sortValue)
	    		.queryParam(SearchUsersConstants.ORDER, ordervalue)
	    	    .queryParam(SearchUsersConstants.PERPAGE, 1)
	    	    .queryParam(SearchUsersConstants.PAGE, 1)
	    		.build().encode().toUri();  

	    when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
	      .thenThrow(HttpClientErrorException.create(null, HttpStatus.FORBIDDEN, "rate limit exceeded", null, null, null));

	    ResponseEntity response = searchUserServiceImpl.fetchOldestUsers(1);
	    assertEquals(expectedResponse, response);	    
  }
  
}
