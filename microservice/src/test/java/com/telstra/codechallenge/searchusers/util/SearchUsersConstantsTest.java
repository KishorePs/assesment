package com.telstra.codechallenge.searchusers.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SearchUsersConstantsTest {

  @Mock
  SearchUsersConstants searchUsersConstants;
  
  @Test
  public void SearchusersConstantsTest() {
    
	  searchUsersConstants = new SearchUsersConstants();
	  assertEquals(searchUsersConstants.ORDER, searchUsersConstants.ORDER);
	  assertEquals(searchUsersConstants.QUALIFIER, searchUsersConstants.QUALIFIER);
	  assertEquals(searchUsersConstants.SORT, searchUsersConstants.SORT);
	  assertEquals(searchUsersConstants.PAGE, searchUsersConstants.PAGE);
	  assertEquals(searchUsersConstants.PERPAGE, searchUsersConstants.PERPAGE);
  }
  
}
